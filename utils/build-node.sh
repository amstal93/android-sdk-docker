#!/usr/bin/env bash

## Get version and build NodeJS variants
function build_node() {
    local prefix template aliases=() build_args=() OPTIND BASE_IMAGE
    while getopts ":b:p:a:T:j" option; do
        case $option in
            b)
                BASE_IMAGE=$OPTARG >&2
                build_args+=("-b" "BASE_IMAGE=$BASE_IMAGE")
                ;;
            p)
                prefix=$OPTARG >&2
                ;;
            a)
                aliases+=("-a" "$OPTARG") >&2
                ;;
            j)
                build_args=("-b" "JDK8_MIRROR=deb http://security.debian.org/debian-security stretch/updates main") >&2
                ;;
            T)
                template=$OPTARG >&2
                ;;
            \?)
                echo "invalid option -$OPTARG"
                exit 1
                ;;
        esac
    done
    version=$(docker run --rm "$BASE_IMAGE" sh -c 'echo "$NODE_VERSION"')
    major=$(echo "$version" | cut -d. -f1)
    minor=$(echo "$version" | cut -d. -f2)
    patch=$(echo "$version" | cut -d. -f3)
    if [[ -n "$template" ]]; then
        # apply suffix to all aliases -> even indexes should be "-a" while odd indexes should be the value
        for (( i=1; i<${#aliases[@]}; i=((i+2)) )); do
            aliases[$i]="${aliases[$i]}-$template"
        done
        aliases+=("-a" "${prefix}-${major}.${minor}-$template")
        aliases+=("-a" "${prefix}-${major}.${minor}.${patch}-$template")
        bin/builder -p "${prefix}-${major}-$template" "${aliases[@]}" -T "$template" "${build_args[@]}"
    else
        aliases+=("-a" "${prefix}-${major}.$minor")
        aliases+=("-a" "${prefix}-${major}.${minor}.$patch")
        bin/builder -p "${prefix}-$major" "${aliases[@]}" -T debian "${build_args[@]}"
    fi
}

build_node "$@"
