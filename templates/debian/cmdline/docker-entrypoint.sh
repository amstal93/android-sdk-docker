#!/bin/bash

if [[ -n "$ASDK_EXTRA_LANGS" ]]; then
    echo "${ASDK_EXTRA_LANGS//,/ }" | xargs locale-gen
fi

exec "$@"
