#!/usr/bin/env sh

while getopts ":o:n:p:s:a:" option; do
    case $option in
        o)
            old_name=$OPTARG >&2
            ;;
        n)
            new_name=$OPTARG >&2
            ;;
        p)
            prefix=$OPTARG >&2
            ;;
        s)
            suffixes=$OPTARG >&2
            ;;
        a)
            aliases=$OPTARG >&2
            ;;
        \?)
            echo "invalid option -$OPTARG"
            exit 1
            ;;
    esac
done

for suffix in ${suffixes//,/ }; do
    for base_name in $prefix ${aliases//,/ }; do
        docker tag "$old_name:${prefix}-$suffix" "$new_name:${base_name}-$suffix"
    done
    docker image rm "$old_name:${prefix}-$suffix"
done
